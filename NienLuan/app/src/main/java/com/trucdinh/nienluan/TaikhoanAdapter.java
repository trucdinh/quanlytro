package com.trucdinh.nienluan;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class TaikhoanAdapter extends BaseAdapter {
    public TaikhoanAdapter(TaiKhoan context, int layout, List<DaidienGET> daidienList) {
        this.context = context;
        this.layout = layout;
        this.daidienList = daidienList;
    }

    private TaiKhoan context;
    private int layout;
    private List<DaidienGET> daidienList;
    @Override
    public int getCount() {
        return daidienList.size();
    }
    private class ViewHolder{
        TextView xinchao, ttsdt, ttcmnd, ttgt,ttngay,ttdc;
        Button EditTT;
    }
    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout, null);
            holder.xinchao = (TextView) view.findViewById(R.id.xinchao);
            holder.ttsdt = (TextView) view.findViewById(R.id.ttsdt);
            holder.ttcmnd = (TextView) view.findViewById(R.id.ttcmnd);
            holder.ttgt = (TextView) view.findViewById(R.id.ttgt);
            holder.ttngay = (TextView) view.findViewById(R.id.ttngay);
            holder.ttdc = (TextView) view.findViewById(R.id.ttdc);
            holder.EditTT = (Button) view.findViewById(R.id.EditTT);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        DaidienGET daidienGET = daidienList.get(i);
        holder.xinchao.setText("Xin chào "+daidienGET.getHoten());
        holder.ttsdt.setText("SĐT: " + daidienGET.getSdt());
        holder.ttcmnd.setText("CMND/CCCD: " + daidienGET.getCmnd());
        holder.ttgt.setText("Giới tính: " + daidienGET.getGioitinh());
        holder.ttngay.setText("Ngày sinh:  "+daidienGET.getNgaysinh());
        holder.ttdc.setText("Đại chỉ: "+daidienGET.getDiachi());
        //bat  su kiện xóa và sửa
        holder.EditTT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, UpdateTaikhoan.class);
                intent.putExtra("dataTaikhoaan", daidienGET);
                context.startActivity(intent);
            }
        });

        return view;
       }
}
