package com.trucdinh.nienluan;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

public class PhongAdapter extends BaseAdapter {
    public PhongAdapter(Phong context, int layout, List<PhongGET> phongList) {
        this.context = context;
        this.layout = layout;
        this.phongList = phongList;
    }

    private Phong context;
    private int layout;
    private List<PhongGET> phongList;
    @Override
    public int getCount() {
        return phongList.size();
    }
    private class ViewHolder{
        TextView tenphong, tinhtrang;
        ImageView btnEditphong, btnDelphong;
    }
    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        PhongAdapter.ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout, null);
            holder.tenphong = (TextView) view.findViewById(R.id.tenphong);
            holder.tinhtrang = (TextView) view.findViewById(R.id.tinhtrang);
            holder.btnDelphong = (ImageView) view.findViewById(R.id.imDelphong);
            holder.btnEditphong = (ImageView) view.findViewById(R.id.imEditphong);
            view.setTag(holder);
        } else {
            holder = (PhongAdapter.ViewHolder) view.getTag();
        }
        PhongGET phongGET = phongList.get(i);
        holder.tenphong.setText(phongGET.getTenphong());
        if(phongGET.getTinhtrang()==1){
            holder.tinhtrang.setText("Tình trạng: còn phòng");
        }else
            holder.tinhtrang.setText("Tình trạng: không còn phòng");

        //bat  su kiện xóa và sửa
        holder.btnEditphong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, UpdatePhong.class);
                intent.putExtra("dataPhong", phongGET);
                context.startActivity(intent);
            }
        });
        holder.btnDelphong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Xacnhanxoa(phongGET.getTenphong(), phongGET.getId_phong());
            }
        });
        return view;
    }
    private void Xacnhanxoa (String ten, int idp){
        AlertDialog.Builder dialogXoa = new AlertDialog.Builder(context);
        dialogXoa.setMessage("Bạn có chắc muốn xóa " +ten+ "?");
        dialogXoa.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                context.DelLP(idp);
            }
        });
        dialogXoa.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogXoa.show();
    }
}
