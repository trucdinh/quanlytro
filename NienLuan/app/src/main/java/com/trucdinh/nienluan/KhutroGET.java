package com.trucdinh.nienluan;

import java.io.Serializable;

public class KhutroGET implements Serializable {
    public KhutroGET(int id_kt, String ten_kt, String diachi, int sophong, int quangcao, String mota, String chuky_noptien) {
        this.id_kt = id_kt;
        this.ten_kt = ten_kt;
        this.diachi = diachi;
        this.sophong = sophong;
        this.quangcao = quangcao;
        this.mota = mota;
        this.chuky_noptien = chuky_noptien;
    }

    public int getId_kt() {
        return id_kt;
    }

    public void setId_kt(int id_kt) {
        this.id_kt = id_kt;
    }

    public String getTen_kt() {
        return ten_kt;
    }

    public void setTen_kt(String ten_kt) {
        this.ten_kt = ten_kt;
    }

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }

    public int getSophong() {
        return sophong;
    }

    public void setSophong(int sophong) {
        this.sophong = sophong;
    }

    public int getQuangcao() {
        return quangcao;
    }

    public void setQuangcao(int quangcao) {
        this.quangcao = quangcao;
    }

    public String getMota() {
        return mota;
    }

    public void setMota(String mota) {
        this.mota = mota;
    }

    public String getChuky_noptien() {
        return chuky_noptien;
    }

    public void setChuky_noptien(String chuky_noptien) {
        this.chuky_noptien = chuky_noptien;
    }

    int id_kt;
    String ten_kt;
    String diachi;
    int sophong;
    int quangcao;
    String mota;
    String chuky_noptien;
}
