
package com.trucdinh.nienluan.Model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class NguoiDung {

    public NguoiDung(String id_tk, String sdt, String matkhau, String phan_quyen, String hoten, String cmnd, String gioitinh, String ngaysinh, String diachi) {
        this.id_tk = id_tk;
        this.sdt = sdt;
        this.matkhau = matkhau;
        this.phan_quyen = phan_quyen;
        this.hoten = hoten;
        this.cmnd = cmnd;
        this.gioitinh = gioitinh;
        this.ngaysinh = ngaysinh;
        this.diachi = diachi;
    }

    public String getId_tk() {
        return id_tk;
    }

    public void setId_tk(String id_tk) {
        this.id_tk = id_tk;
    }

    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }

    public String getMatkhau() {
        return matkhau;
    }

    public void setMatkhau(String matkhau) {
        this.matkhau = matkhau;
    }

    public String getPhan_quyen() {
        return phan_quyen;
    }

    public void setPhan_quyen(String phan_quyen) {
        this.phan_quyen = phan_quyen;
    }

    public String getHoten() {
        return hoten;
    }

    public void setHoten(String hoten) {
        this.hoten = hoten;
    }

    public String getCmnd() {
        return cmnd;
    }

    public void setCmnd(String cmnd) {
        this.cmnd = cmnd;
    }

    public String getGioitinh() {
        return gioitinh;
    }

    public void setGioitinh(String gioitinh) {
        this.gioitinh = gioitinh;
    }

    public String getNgaysinh() {
        return ngaysinh;
    }

    public void setNgaysinh(String ngaysinh) {
        this.ngaysinh = ngaysinh;
    }

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }

    private String id_tk;
    private String sdt;
    private String matkhau;
    private String phan_quyen;
    private String hoten;
    private String cmnd;
    private String gioitinh;
    private String ngaysinh;
    private String diachi;



}