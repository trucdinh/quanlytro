package com.trucdinh.nienluan.Service;

import com.trucdinh.nienluan.Model.NguoiDung;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface apiInterface {
    @FormUrlEncoded
    @POST("getNguoidung.php")
    Call<NguoiDung> getNguoidung(
            @Field("sdt")  String sdt
            //, @Field("matkhau") String matkhau
    );


}
