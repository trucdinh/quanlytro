package com.trucdinh.nienluan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class UpdateTaikhoan extends AppCompatActivity {
    EditText udhoten,udgioitinh,udngaysinh,uddiachi,udcmnd;
    Button  btnCapnhatTK,btnHuy;
    String urlUpdate="http://192.168.1.7/nienluan/updateTaikhoan.php";
    int id;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_updatetaiikhoan);
        Intent intent=getIntent();
        DaidienGET daidienGET=(DaidienGET) intent.getSerializableExtra("dataTaikhoaan");

        udhoten=(EditText) findViewById(R.id.udhoten);
        udgioitinh=(EditText) findViewById(R.id.udgioitinh);
        udngaysinh=(EditText) findViewById(R.id.udngaysinh);
        uddiachi=(EditText) findViewById(R.id.uddiachi);
        udcmnd=(EditText) findViewById(R.id.udcmnd);
        btnCapnhatTK=(Button) findViewById(R.id.btnCapnhatTK);
        btnHuy=(Button) findViewById(R.id.btnHuy);
        id=daidienGET.getId_tk();

        udhoten.setText(daidienGET.getHoten());
        uddiachi.setText(daidienGET.getDiachi());
        udgioitinh.setText(daidienGET.getGioitinh());
        udngaysinh.setText(daidienGET.getNgaysinh());
        udcmnd.setText(daidienGET.getCmnd());
        btnCapnhatTK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(udhoten.getText().toString().isEmpty()){
                    Toast.makeText(UpdateTaikhoan.this, "Bạn chưa nhập TÊN!", Toast.LENGTH_SHORT).show();
                    udhoten.requestFocus();
                }else if(udcmnd.getText().toString().isEmpty()){
                    Toast.makeText(UpdateTaikhoan.this, "Bạn chưa nhập Chứng minh", Toast.LENGTH_SHORT).show();
                    udcmnd.requestFocus();
                }else if(udngaysinh.getText().toString().isEmpty()){
                    Toast.makeText(UpdateTaikhoan.this, "Bạn chưa nhập Ngày sinh", Toast.LENGTH_SHORT).show();
                    udngaysinh.requestFocus();
                }else if(uddiachi.getText().toString().isEmpty()){
                    Toast.makeText(UpdateTaikhoan.this, "Bạn chưa nhập Địa chỉ", Toast.LENGTH_SHORT).show();
                    uddiachi.requestFocus();
                }else if(udgioitinh.getText().toString().isEmpty()){
                    Toast.makeText(UpdateTaikhoan.this, "Bạn chưa nhập Giới tính", Toast.LENGTH_SHORT).show();
                    udgioitinh.requestFocus();
                }else {
                    CapnhatTaikhoan(urlUpdate);
                }
            }

        });
    }
    private void CapnhatTaikhoan(String url){
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.trim().equals("success")){
                    Toast.makeText(UpdateTaikhoan.this, "Cập nhật thành công", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(UpdateTaikhoan.this,TaiKhoan.class));
                }else{
                    Toast.makeText(UpdateTaikhoan.this, "Lỗi cập nhật", Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params=new HashMap<>();
                params.put("id_tk", String.valueOf(id));
                params.put("hoten",udhoten.getText().toString().trim());
                params.put("cmnd",udcmnd.getText().toString().trim());
                params.put("gioitinh",udgioitinh.getText().toString().trim());
                params.put("ngaysinh",udngaysinh.getText().toString().trim());
                params.put("diachi",uddiachi.getText().toString().trim());
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
