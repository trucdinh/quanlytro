package com.trucdinh.nienluan;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TrangChu extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar_tc;
    ImageButton btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn10, btn11, btn12;
    TextView sltro,slphong,slnguoithue,slphongtrong;
public static String tkdn;
    //mang loại phong
    //public  static List<LoaiphongGET> loaiphonglist;
    public static ArrayList<LoaiphongGET> loaiphonglist = new ArrayList<>();
    String urlData="http://192.168.1.7/nienluan/listLoaiphong.php";
    String countKhutro="http://192.168.1.7/nienluan/countKhutro.php";
    String countPhong="http://192.168.1.7/nienluan/countPhong.php";
    String countPhongtrong="http://192.168.1.7/nienluan/countPhongtrong.php";
    String countkhachthue="http://192.168.1.7/nienluan/countkhachthue.php";
@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_trangchu);
        toolbar_tc = (Toolbar) findViewById(R.id.toolbar_tc);
        setSupportActionBar(toolbar_tc);
        //loai bo tieu đe co  san
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //toolbar_tc.inflateMenu(R.menu.menu_tc);
        btn1 = (ImageButton) findViewById(R.id.imbtn1);
        btn2 = (ImageButton) findViewById(R.id.imbtn2);
        btn3 = (ImageButton) findViewById(R.id.imbtn3);
        btn4 = (ImageButton) findViewById(R.id.imbtn4);
        btn5 = (ImageButton) findViewById(R.id.imbtn5);
        btn6 = (ImageButton) findViewById(R.id.imbtn6);
        btn7 = (ImageButton) findViewById(R.id.imbtn7);
        btn8 = (ImageButton) findViewById(R.id.imbtn8);
        btn9 = (ImageButton) findViewById(R.id.imbtn9);
        btn10 = (ImageButton) findViewById(R.id.imbtn10);
        btn11 = (ImageButton) findViewById(R.id.imbtn11);
        btn12 = (ImageButton) findViewById(R.id.imbtn12);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn12.setOnClickListener(this);
        btn5.setOnClickListener(this);
        slnguoithue=(TextView)findViewById(R.id.slnguoithue);
         sltro=(TextView)findViewById(R.id.sltro);
         slphong=(TextView)findViewById(R.id.slphong);
         slphongtrong=(TextView)findViewById(R.id.slphongtrong);
        //nhận id
        Intent intent=getIntent();
        tkdn=intent.getStringExtra("id_tkdn");
        //Toast.makeText(TrangChu.this, tkdn, Toast.LENGTH_SHORT).show();

    countKhutro(countKhutro);
    countPhong(countPhong);
    countPhongttrong(countPhongtrong);
    countKhachthue(countkhachthue);

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imbtn1:
                Intent intent1 = new Intent(TrangChu.this, ToaNha.class);
                intent1.putExtra("id_tkdn1",tkdn);
                startActivity(intent1);

                break;
            case R.id.imbtn2:
                //goi cai API PHP nhận về cái danh sach loại phong

                Intent intent2 = new Intent(TrangChu.this, Phong.class);
               // intent2.putExtra("id_tkdn1",tkdn);
                //intent2.putExtra("mangloaiphong",loaiphonglist);
                startActivity(intent2);

                break;
            case R.id.imbtn3:
                Intent intent3 = new Intent(TrangChu.this, LoaiPhong.class);
                startActivity(intent3);
                break;
            case R.id.imbtn7:
                Intent intent4 = new Intent(TrangChu.this, DaiDien.class);
                startActivity(intent4);
                break;
            case R.id.imbtn12:
                Intent intent12 = new Intent(TrangChu.this, TaiKhoan.class);
                intent12.putExtra("id_tkdn1",tkdn);
                startActivity(intent12);
                break;
            case R.id.imbtn5:
                Intent intent5 = new Intent(TrangChu.this, DichVu.class);
                startActivity(intent5);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tc, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
//thao tác với các item trong menu
        switch (id) {
            case R.id.xem:
                Toast.makeText(TrangChu.this, "Bạn vừa ấn vào" + item.getTitle(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.dangxuat:
                Toast.makeText(TrangChu.this, "Bạn vừa ấn vào" + item.getTitle(), Toast.LENGTH_SHORT).show();
                break;
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
//    private void getLoaiphong(String url){
//        RequestQueue requestQueue= Volley.newRequestQueue(this);
//        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
//            @Override
//            public void onResponse(JSONArray response) {
//                loaiphonglist.clear();
//                for (int i=0;i<response.length();i++){
//                    try {
//                        JSONObject object =  response.getJSONObject(i);
//                        loaiphonglist.add(new LoaiphongGET(
//                                object.getInt("id_loai"),
//                                object.getString("tenloai"),
//                                object.getString("dientich"),
//                                object.getInt("soluong_khach"),
//                                object.getString("doituong"),
//                                object.getString("luu_y")
//                        ));
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//                //adapter.notifyDataSetChanged();
//            }
//        },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(TrangChu.this, "Lỗi tải nội dung", Toast.LENGTH_SHORT).show();
//                    }
//                }
//        );
//        requestQueue.add(jsonArrayRequest);
//    }

    public void countPhong (String url){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response!=null) {
                    sltro.setText(response);
                   } else {
                    Toast.makeText(TrangChu.this, "Chưa lấy đc số lượng khu trọ", Toast.LENGTH_SHORT).show();

                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //bao loi cho nguoi lap trinh
                        Toast.makeText(TrangChu.this, "Xảy ra lỗi lấy ID", Toast.LENGTH_SHORT).show();
                        Log.d("AAA", "Lỗi!\n" + error.toString());
                    }
                }
        ) {
            };
        requestQueue.add(stringRequest);

    }
    public void countKhutro (String url){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response!=null) {
                    slphong.setText(response);
                } else {
                    Toast.makeText(TrangChu.this, "Chưa lấy đc số lượng phòng", Toast.LENGTH_SHORT).show();

                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //bao loi cho nguoi lap trinh
                        Toast.makeText(TrangChu.this, "Xảy ra lỗi lấy ID", Toast.LENGTH_SHORT).show();
                        Log.d("AAA", "Lỗi!\n" + error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_tk", tkdn);
                return params;
            }
        };
        requestQueue.add(stringRequest);

    }
    public void countPhongttrong (String url){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response!=null) {
                    slphongtrong.setText(response);
                } else {
                    Toast.makeText(TrangChu.this, "Chưa lấy đc số lượng phòng", Toast.LENGTH_SHORT).show();

                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //bao loi cho nguoi lap trinh
                        Toast.makeText(TrangChu.this, "Xảy ra lỗi lấy ID", Toast.LENGTH_SHORT).show();
                        Log.d("AAA", "Lỗi!\n" + error.toString());
                    }
                }
        ) {

        };
        requestQueue.add(stringRequest);

    }
    public void countKhachthue (String url){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response!=null) {
                    slnguoithue.setText(response);
                } else {
                    Toast.makeText(TrangChu.this, "Chưa lấy đc số lượng phòng", Toast.LENGTH_SHORT).show();

                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //bao loi cho nguoi lap trinh
                        Toast.makeText(TrangChu.this, "Xảy ra lỗi lấy ID", Toast.LENGTH_SHORT).show();
                        Log.d("AAA", "Lỗi!\n" + error.toString());
                    }
                }
        ) {

        };
        requestQueue.add(stringRequest);

    }
}
