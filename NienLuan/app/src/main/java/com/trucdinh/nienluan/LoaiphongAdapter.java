package com.trucdinh.nienluan;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class LoaiphongAdapter extends BaseAdapter {
    private LoaiPhong context;
    private int layout;
    private List<LoaiphongGET> loaiphongList;
    public LoaiphongAdapter(LoaiPhong context, int layout, ArrayList<LoaiphongGET> loaiphongList) {
        this.context = context;
        this.layout = layout;
        this.loaiphongList = loaiphongList;
    }




    @Override
    public int getCount() {
        return loaiphongList.size();
    }
    private class ViewHolder{
        TextView tenloai, soluong_khach, dientich, doituong,txtgia;
        ImageView imEdit, imDel;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LoaiphongAdapter.ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout, null);
            holder.tenloai = (TextView) view.findViewById(R.id.txttenloaiphong);
            holder.dientich = (TextView) view.findViewById(R.id.txtdientich);
            holder.soluong_khach = (TextView) view.findViewById(R.id.txtsoluongkhach);
            holder.doituong = (TextView) view.findViewById(R.id.txtdoituong);
            holder.txtgia = (TextView) view.findViewById(R.id.txtgia);

            holder.imDel = (ImageView) view.findViewById(R.id.imDelLP);
            holder.imEdit = (ImageView) view.findViewById(R.id.imEditLP);
            view.setTag(holder);
        } else {
            holder = (LoaiphongAdapter.ViewHolder) view.getTag();
        }
        LoaiphongGET loaiphongGET = loaiphongList.get(i);
        holder.tenloai.setText(loaiphongGET.getTenloai());
        holder.soluong_khach.setText("SL tối đa: " + loaiphongGET.getSoluong_khach() + " người");
        holder.dientich.setText("Diện tích: " + loaiphongGET.getDientich());
        holder.doituong.setText("Đối tương: " + loaiphongGET.getDoituong());
        holder.txtgia.setText(""+loaiphongGET.getGia_phong());

        //bat  su kiện xóa và sửa
        holder.imEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, UpdateLoaiphong.class);
                intent.putExtra("dataLoaiphong", loaiphongGET);
                context.startActivity(intent);
            }
        });
        holder.imDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Xacnhanxoa(loaiphongGET.getTenloai(), loaiphongGET.getId_loai());
            }
        });
        return view;
    }
    private void Xacnhanxoa (String ten, int idlp){
        AlertDialog.Builder dialogXoa = new AlertDialog.Builder(context);
        dialogXoa.setMessage("Bạn có chắc muốn xóa " +ten+ "?");
        dialogXoa.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                context.DelLP(idlp);
            }
        });
        dialogXoa.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogXoa.show();
    }
}
