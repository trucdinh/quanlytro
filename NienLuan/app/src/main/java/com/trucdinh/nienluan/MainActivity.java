package com.trucdinh.nienluan;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.trucdinh.nienluan.Model.NguoiDung;
import com.trucdinh.nienluan.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.trucdinh.nienluan.Model.NguoiDung;
import com.trucdinh.nienluan.Service.APIClient;
import com.trucdinh.nienluan.Service.apiInterface;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

Button btnDangnhap, btnDangky;
EditText  sdt, matkhau;
private NguoiDung NguoiDung;
String urlData ="http://192.168.1.7/nienluan/getNguoidung.php";
String urlGetid="http://192.168.1.7/nienluan/getID.php";
public static String id_tkdn;
private NguoiDung nguoiDung;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnDangnhap=(Button) findViewById(R.id.btnDangnhap);
        sdt=(EditText) findViewById(R.id.sdt);
        matkhau=(EditText)findViewById(R.id.matkhau);
        btnDangky=(Button) findViewById(R.id.btnDangky);
        btnDangnhap.setOnClickListener(this);
        btnDangky.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent =new Intent(MainActivity.this, Dangky.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(sdt.getText().toString().isEmpty()){
            Toast.makeText(this, "Nhập tên đăng nhập", Toast.LENGTH_SHORT).show();
            sdt.requestFocus();
        }else if(matkhau.getText().toString().isEmpty()){
            Toast.makeText(this, "Nhập mật khẩu", Toast.LENGTH_SHORT).show();
            matkhau.requestFocus();
        }else {
            Login(urlData);
            getID(urlGetid);
        }
    }
    private void Login (String url) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response.equals("success")) {
                    Toast.makeText(MainActivity.this, "Đăng nhập  thành công", Toast.LENGTH_SHORT).show();
                   // id_tkdn = response;
                   // startActivity(new Intent(MainActivity.this, TrangChu.class));
                } else {
                    Toast.makeText(MainActivity.this, "Đăng nhập thất bại", Toast.LENGTH_SHORT).show();

                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //bao loi cho nguoi lap trinh
                        Toast.makeText(MainActivity.this, "Xảy ra lỗi lập trình", Toast.LENGTH_SHORT).show();
                        Log.d("AAA", "Lỗi!\n" + error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("sdt", sdt.getText().toString().trim());
                params.put("matkhau", matkhau.getText().toString().trim());
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
    public void getID (String url){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response!=null) {
                    id_tkdn = response;
                   // Toast.makeText(MainActivity.this, id_tkdn, Toast.LENGTH_SHORT).show();

                    Intent  intent =new Intent(MainActivity.this, TrangChu.class);
                    intent.putExtra("id_tkdn",id_tkdn);
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "Chưa lấy đc id", Toast.LENGTH_SHORT).show();

                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //bao loi cho nguoi lap trinh
                        Toast.makeText(MainActivity.this, "Xảy ra lỗi lấy ID", Toast.LENGTH_SHORT).show();
                        Log.d("AAA", "Lỗi!\n" + error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("sdt", sdt.getText().toString().trim());
                params.put("matkhau", matkhau.getText().toString().trim());
                return params;
            }
        };
        requestQueue.add(stringRequest);

    }
    }
