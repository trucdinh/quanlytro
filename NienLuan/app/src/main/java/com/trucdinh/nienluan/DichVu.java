package com.trucdinh.nienluan;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewParent;

public class DichVu extends AppCompatActivity {
    private ViewPager viewPager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_dichvu);
        viewPager=(ViewPager) findViewById(R.id.viewdichvu);
        viewPager.setAdapter(new DichvuAdapter(getSupportFragmentManager()));
        TabLayout tabLayout=(TabLayout) findViewById(R.id.tabdichvu);
        tabLayout.setupWithViewPager(viewPager);

    }


}
