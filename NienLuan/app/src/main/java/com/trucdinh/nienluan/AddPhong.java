package com.trucdinh.nienluan;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.w3c.dom.Text;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddPhong extends AppCompatActivity {
    Spinner spinLoaiphong;
    EditText edtenphong, edluuyPhong;
    CheckBox cbtinhtrangco,cbtinhtrangkhong;
   // ImageView imphong;
    Button themphong,huyphong;
    TextView selection;
    int idlp=8, idkt=1, tinhtrang;
   // int Request_Code_Image=123;
    String urlInsert="http://192.168.1.7/nienluan/insertPhong.php";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_addphong);
        spinLoaiphong = (Spinner) findViewById(R.id.spinLoaiphong);
        cbtinhtrangco = (CheckBox) findViewById(R.id.cbtinhtrangco);
        cbtinhtrangkhong = (CheckBox) findViewById(R.id.cbtinhtrangkhong);
        edluuyPhong = (EditText) findViewById(R.id.edluuyPhong);
        edtenphong = (EditText) findViewById(R.id.edtenphong);
        // imphong=(ImageView)findViewById(R.id.imphong);
        themphong = (Button) findViewById(R.id.themphong);
        huyphong = (Button) findViewById(R.id.huyphong);

//        String loaiphong;
//        Intent intent=getIntent();
//        loaiphong=intent.getStringExtra("mangloaiphong");
//        Toast.makeText(AddPhong.this, ""+arr, Toast.LENGTH_SHORT).show();

        String arr[] = {
                "Có gác",
                "Không gác",
                "Mini house"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item,
                arr);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinLoaiphong.setAdapter(adapter);
        spinLoaiphong.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //selection.setText(arr[2]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        themphong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ThemPhong(urlInsert);
            }
        });
        cbtinhtrangco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((CheckBox) v).isChecked();
                // Check which checkbox was clicked
                if (checked){
                    tinhtrang=1;
                }
                else{
                    tinhtrang=0;
                }
            }
        });
        cbtinhtrangkhong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((CheckBox) v).isChecked();
                // Check which checkbox was clicked
                if (checked){
                    tinhtrang=0;
                }
                else{
                    tinhtrang=1;
                }
            }
        });
    }
    private void ThemPhong (String url){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.trim().equals("true")){

                    Toast.makeText(AddPhong.this, "Thêm lỗi", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(AddPhong.this, "Thêm thành công!", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(AddPhong.this,Phong.class));
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //bao loi cho nguoi lap trinh
                        Toast.makeText(AddPhong.this, "Xảy ra lỗi lập trình", Toast.LENGTH_SHORT).show();
                        Log.d("AAA","Lỗi!\n"  + error.toString());
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("id_kt",String.valueOf(idkt));
                params.put("id_loai",String.valueOf(idlp));
                params.put("tenphong",edtenphong.getText().toString().trim());
                params.put("tinhtrang",String.valueOf(tinhtrang));
                params.put("luu_y",edluuyPhong.getText().toString().trim());
          return params;
            }
        };
        requestQueue.add(stringRequest);
    }




















































//        imphong.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(Intent.ACTION_PICK);
//                intent.setType("image/*");
//                startActivityForResult(intent,Request_Code_Image);
//            }
//        });
 //   }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//       if(requestCode==Request_Code_Image && requestCode ==RESULT_OK  && data!=null){
//           Uri uri =data.getData();
//
//           try {
//               InputStream inputStream =getContentResolver().openInputStream(uri);
//               Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
//               imphong.setImageBitmap(bitmap);
//           } catch (FileNotFoundException e) {
//               e.printStackTrace();
//           }
//       }
//        super.onActivityResult(requestCode, resultCode, data);
//    }
}
