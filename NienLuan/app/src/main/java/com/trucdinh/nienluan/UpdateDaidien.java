package com.trucdinh.nienluan;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class UpdateDaidien extends AppCompatActivity {
    EditText udtendaidien,udsdt,udcmnd,udgioitinh,udngaysinh,uddiachi;
    Button btnCapnhat, btnHuy;
    int id=0, phanquyen=2;
    String urlInsert="http://192.168.1.7/nienluan/updateDaidien.php";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_updatedaidien);
            Intent intent=getIntent();
            DaidienGET daidienGET=(DaidienGET) intent.getSerializableExtra("dataDaidien");
        //Toast.makeText(this, daidienGET.getCmnd(), Toast.LENGTH_SHORT).show();
            udtendaidien=(EditText) findViewById(R.id.udtendaidien);
            udsdt=(EditText) findViewById(R.id.udsdt);
            udcmnd=(EditText) findViewById(R.id.udcmnd);
            udgioitinh=(EditText) findViewById(R.id.udgioitinh);
            udngaysinh=(EditText) findViewById(R.id.udngaysinh);
            uddiachi=(EditText) findViewById(R.id.uddiachi);
            btnCapnhat=(Button) findViewById(R.id.btnCapnhat);
            btnHuy=(Button) findViewById(R.id.btnHuy);

            id=daidienGET.getId_tk();
            udtendaidien.setText(daidienGET.getHoten());
            udsdt.setText(daidienGET.getSdt());
            udcmnd.setText(daidienGET.getCmnd());
            udgioitinh.setText(daidienGET.getGioitinh());
            udngaysinh.setText(daidienGET.getNgaysinh());
            uddiachi.setText(daidienGET.getDiachi());

            btnCapnhat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(udtendaidien.getText().toString().isEmpty()){
                        Toast.makeText(UpdateDaidien.this, "Bạn chưa nhập TÊN!", Toast.LENGTH_SHORT).show();
                        udtendaidien.requestFocus();
                    }else if(udsdt.getText().toString().isEmpty()){
                        Toast.makeText(UpdateDaidien.this, "Bạn chưa nhập SĐT", Toast.LENGTH_SHORT).show();
                        udsdt.requestFocus();
                    }else if(udcmnd.getText().toString().isEmpty()){
                        Toast.makeText(UpdateDaidien.this, "Bạn chưa nhập Chứng minh", Toast.LENGTH_SHORT).show();
                        udcmnd.requestFocus();
                    }else if(udngaysinh.getText().toString().isEmpty()){
                        Toast.makeText(UpdateDaidien.this, "Bạn chưa nhập Ngày sinh", Toast.LENGTH_SHORT).show();
                        udngaysinh.requestFocus();
                    }else if(uddiachi.getText().toString().isEmpty()){
                        Toast.makeText(UpdateDaidien.this, "Bạn chưa nhập Địa chỉ", Toast.LENGTH_SHORT).show();
                        uddiachi.requestFocus();
                    }else if(udgioitinh.getText().toString().isEmpty()){
                        Toast.makeText(UpdateDaidien.this, "Bạn chưa nhập Giới tính", Toast.LENGTH_SHORT).show();
                        udgioitinh.requestFocus();
                    }else {
                        CapnhatDaidien(urlInsert);
                    }
                }

            });
            btnHuy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(UpdateDaidien.this,DaiDien.class));
                }
            });
    }
    private void CapnhatDaidien(String url){
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.trim().equals("success")){
                    Toast.makeText(UpdateDaidien.this, "Cập nhật thành công", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(UpdateDaidien.this,DaiDien.class));
                }else{
                    Toast.makeText(UpdateDaidien.this, "Lỗi cập nhật", Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params=new HashMap<>();
                params.put("id_tk", String.valueOf(id));
                params.put("sdt",udsdt.getText().toString().trim());
                params.put("phan_quyen", String.valueOf(phanquyen));
                params.put("hoten",udtendaidien.getText().toString().trim());
                params.put("cmnd",udcmnd.getText().toString().trim());
                params.put("gioitinh",udgioitinh.getText().toString().trim());
                params.put("ngaysinh",udngaysinh.getText().toString().trim());
                params.put("diachi",uddiachi.getText().toString().trim());
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
