package com.trucdinh.nienluan;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class dvcpAdapter extends BaseAdapter {
    public dvcpAdapter(DVCP context, int layout, List<dvcpGET> dvcpList) {
        this.context = context;
        this.layout = layout;
        this.dvcpList = dvcpList;
    }

    private DVCP context;
    private int layout;
    private List<dvcpGET> dvcpList;
    @Override
    public int getCount() {
        return dvcpList.size();
    }
    private class ViewHolder{
        TextView tendvcp, donvitinh;
        ImageView imEditdvcp, imDeldvcp;
    }
    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        dvcpAdapter.ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout, null);
            holder.tendvcp = (TextView) view.findViewById(R.id.tendvcp);
            holder.donvitinh = (TextView) view.findViewById(R.id.donvitinh);


            holder.imEditdvcp = (ImageView) view.findViewById(R.id.imEditdvcp);
            holder.imDeldvcp = (ImageView) view.findViewById(R.id.imDeldvcp);
            view.setTag(holder);
        } else {
            holder = (dvcpAdapter.ViewHolder) view.getTag();
        }
        dvcpGET dvcpGET = dvcpList.get(i);
        holder.tendvcp.setText(dvcpGET.getTendv());
        holder.donvitinh.setText("Đơn vị tính " + dvcpGET.getDonvi() + " người");


//        //bat  su kiện xóa và sửa
//        holder.imEdit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(context, UpdateLoaiphong.class);
//                intent.putExtra("dataLoaiphong", loaiphongGET);
//                context.startActivity(intent);
//            }
//        });
//        holder.imDel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Xacnhanxoa(loaiphongGET.getTenloai(), loaiphongGET.getId_loai());
//            }
//        });
        return view;
    }
//    private void Xacnhanxoa (String ten, int idlp){
//        AlertDialog.Builder dialogXoa = new AlertDialog.Builder(context);
//        dialogXoa.setMessage("Bạn có chắc muốn xóa " +ten+ "?");
//        dialogXoa.setPositiveButton("Có", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                context.DelLP(idlp);
//            }
//        });
//        dialogXoa.setNegativeButton("Không", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//            }
//        });
//        dialogXoa.show();
//    }
}
