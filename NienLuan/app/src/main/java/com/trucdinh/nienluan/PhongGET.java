package com.trucdinh.nienluan;

import java.io.Serializable;

public class PhongGET implements Serializable {
    public PhongGET(int id_phong, String tenphong, int tinhtrang, String hinhanh, String luu_y) {
        this.id_phong = id_phong;
        this.tenphong = tenphong;
        this.tinhtrang = tinhtrang;
        this.hinhanh = hinhanh;
        this.luu_y = luu_y;
    }

    public int getId_phong() {
        return id_phong;
    }

    public void setId_phong(int id_phong) {
        this.id_phong = id_phong;
    }

    public String getTenphong() {
        return tenphong;
    }

    public void setTenphong(String tenphong) {
        this.tenphong = tenphong;
    }

    public int getTinhtrang() {
        return tinhtrang;
    }

    public void setTinhtrang(int tinhtrang) {
        this.tinhtrang = tinhtrang;
    }

    public String getHinhanh() {
        return hinhanh;
    }

    public void setHinhanh(String hinhanh) {
        this.hinhanh = hinhanh;
    }

    public String getLuu_y() {
        return luu_y;
    }

    public void setLuu_y(String luu_y) {
        this.luu_y = luu_y;
    }

    int id_phong;
    String tenphong;
    int tinhtrang;
    String hinhanh;
    String luu_y;
}
