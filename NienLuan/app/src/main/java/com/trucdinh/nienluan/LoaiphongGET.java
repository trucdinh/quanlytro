package com.trucdinh.nienluan;

import java.io.Serializable;

public class LoaiphongGET implements Serializable {

    public LoaiphongGET(int id_loai, String tenloai, String dientich, int soluong_khach, String doituong, int gia_phong, String luu_y) {
        this.id_loai = id_loai;
        this.tenloai = tenloai;
        this.dientich = dientich;
        this.soluong_khach = soluong_khach;
        this.doituong = doituong;
        this.gia_phong = gia_phong;
        this.luu_y = luu_y;
    }

    public int getId_loai() {
        return id_loai;
    }

    public void setId_loai(int id_loai) {
        this.id_loai = id_loai;
    }

    public String getTenloai() {
        return tenloai;
    }

    public void setTenloai(String tenloai) {
        this.tenloai = tenloai;
    }

    public String getDientich() {
        return dientich;
    }

    public void setDientich(String dientich) {
        this.dientich = dientich;
    }

    public int getSoluong_khach() {
        return soluong_khach;
    }

    public void setSoluong_khach(int soluong_khach) {
        this.soluong_khach = soluong_khach;
    }

    public String getDoituong() {
        return doituong;
    }

    public void setDoituong(String doituong) {
        this.doituong = doituong;
    }

    public int getGia_phong() {
        return gia_phong;
    }

    public void setGia_phong(int gia_phong) {
        this.gia_phong = gia_phong;
    }

    public String getLuu_y() {
        return luu_y;
    }

    public void setLuu_y(String luu_y) {
        this.luu_y = luu_y;
    }

    int id_loai;
    String  tenloai;
    String dientich;
    int soluong_khach;
    String doituong;
    int gia_phong;
    String luu_y;


}
