package com.trucdinh.nienluan;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.trucdinh.nienluan.Model.NguoiDung;

import java.util.HashMap;
import java.util.Map;

public class AddLoaiphong extends AppCompatActivity {
    EditText edtenloai, eddientich, edsoluongkhach, eddoituong, edluuy,edgia;
    Button btnThemloaiphong, btnHuy;
    String urlInsert="http://192.168.1.7/nienluan/insertLoaiphong.php";
    int quangcao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_addloaiphong);

        edtenloai=(EditText) findViewById(R.id.edtenloai);
        eddientich=(EditText) findViewById(R.id.eddientich);
        edsoluongkhach=(EditText) findViewById(R.id.edsoluongkhach);
        eddoituong=(EditText) findViewById(R.id.eddoituong);
        edluuy=(EditText) findViewById(R.id.edluuy);
        edgia=(EditText) findViewById(R.id.edgia);
        btnHuy=(Button) findViewById(R.id.btnHuy);
        btnThemloaiphong=(Button) findViewById(R.id.btnThemloaiphong);


        btnHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1=new Intent(AddLoaiphong.this,LoaiPhong.class);
                startActivity(intent1);
            }
        });
        btnThemloaiphong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtenloai.getText().toString().isEmpty()){
                    Toast.makeText(AddLoaiphong.this, "Bạn chưa nhập tên loại phòng!", Toast.LENGTH_SHORT).show();
                    edtenloai.requestFocus();
                }else if(eddientich.getText().toString().isEmpty()){
                    Toast.makeText(AddLoaiphong.this, "Bạn chưa nhập diện tích", Toast.LENGTH_SHORT).show();
                    eddientich.requestFocus();
                }else if(edsoluongkhach.getText().toString().isEmpty()){
                    Toast.makeText(AddLoaiphong.this, "Bạn chưa nhập số lượng khách", Toast.LENGTH_SHORT).show();
                    edsoluongkhach.requestFocus();
                }else if(eddoituong.getText().toString().isEmpty()){
                    Toast.makeText(AddLoaiphong.this, "Bạn chưa nhập đối tượng thuê phòng", Toast.LENGTH_SHORT).show();
                    eddoituong.requestFocus();
                }else {
                    ThemLoaiphong(urlInsert);
                }
            }
        });




    }
    private void ThemLoaiphong (String url){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.trim().equals("true")){
                    Toast.makeText(AddLoaiphong.this, "Thêm lỗi", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(AddLoaiphong.this, "Thêm thành công!", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(AddLoaiphong.this,LoaiPhong.class));
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //bao loi cho nguoi lap trinh
                        Toast.makeText(AddLoaiphong.this, "Xảy ra lỗi lập trình", Toast.LENGTH_SHORT).show();
                        Log.d("AAA","Lỗi!\n"  + error.toString());
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("tenloai",edtenloai.getText().toString().trim());
                params.put("dientich",eddientich.getText().toString().trim());
                params.put("soluong_khach",edsoluongkhach.getText().toString().trim());
                params.put("doituong",eddoituong.getText().toString().trim());
                params.put("gia_phong",edgia.getText().toString().trim());
                params.put("luu_y",edluuy.getText().toString().trim());

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }


}


