package com.trucdinh.nienluan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Phong extends AppCompatActivity {
    Toolbar toolbar_phong;
    ListView listPhong;
    ArrayList<PhongGET> phongArrayList;
    PhongAdapter adapter;
    String urlData="http://192.168.1.7/nienluan/listPhong.php";
    String urlDel="http://192.168.1.7/nienluan/delPhong.php";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_phong);
        toolbar_phong=(Toolbar) findViewById(R.id.toolbar_phong);
        setSupportActionBar(toolbar_phong);
        //loai bo tieu đe co  san
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        listPhong =(ListView)findViewById(R.id.listphong);
        phongArrayList= new ArrayList<>();
        adapter=new PhongAdapter(Phong.this,R.layout.layout_listphong,phongArrayList);
        listPhong.setAdapter(adapter);
        Hienthi(urlData);
    }
    private void Hienthi(String url){
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                phongArrayList.clear();
                for (int i=0;i<response.length();i++){
                    try {
                        JSONObject object =  response.getJSONObject(i);
                        phongArrayList.add(new PhongGET(
                                object.getInt("id_phong"),
                                object.getString("tenphong"),
                                object.getInt("tinhtrang"),
                                object.getString("hinhanh"),
                                object.getString("luu_y")
                        ));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                adapter.notifyDataSetChanged();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Phong.this, "Lỗi tải nội dung", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }
    public void DelLP(int idp){
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlDel, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.trim().equals("success")){
                    Toast.makeText(Phong.this, "Xóa thành công", Toast.LENGTH_SHORT).show();
                    Hienthi(urlData);
                }else{
                    Toast.makeText(Phong.this, "Xóa không thành công", Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("id_phong", String.valueOf(idp));
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toanha, menu);
        MenuItem timkiem=menu.findItem(R.id.timkiem);
        SearchView search= (SearchView) timkiem.getActionView();


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent = new Intent(Phong.this, AddPhong.class);
        startActivity(intent);
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
