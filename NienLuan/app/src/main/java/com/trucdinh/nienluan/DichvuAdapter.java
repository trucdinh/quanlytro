package com.trucdinh.nienluan;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.widget.BaseAdapter;

public class DichvuAdapter extends FragmentStatePagerAdapter {
    private String listTab[]={"Dịch vụ có phí","Dịch vụ miễn phí"};
    private FragDVCP fragDVCP;
    private  FragDVMP fragDVMP;

    public DichvuAdapter(FragmentManager fm) {
        super(fm);
        fragDVCP=new FragDVCP();
        fragDVMP=new FragDVMP();
    }

    @Override
    public Fragment getItem(int position) {
        if(position==0){
            return fragDVMP;
        }else if (position==1){
            return fragDVCP;

        }
        return null;
    }

    @Override
    public int getCount() {
        return listTab.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return listTab[position];
    }
}
