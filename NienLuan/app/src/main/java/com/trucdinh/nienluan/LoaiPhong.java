package com.trucdinh.nienluan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LoaiPhong extends AppCompatActivity
{
    Toolbar  toolbar_loaiphong;
    ListView listLoaiphong;
    ArrayList<LoaiphongGET> loaiphongArrayList;
    LoaiphongAdapter adapter;
    public static String tkdn;
    String urlData="http://192.168.1.7/nienluan/listLoaiphong.php";
    String urlDel="http://192.168.1.7/nienluan/delLoaiphong.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_loaiphong);
         toolbar_loaiphong=(Toolbar) findViewById(R.id.toolbar_loaiphong);
        setSupportActionBar(toolbar_loaiphong);
        //loai bo tieu đe co  san
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        listLoaiphong =(ListView)findViewById(R.id.listLoaiphong);
        loaiphongArrayList= new ArrayList<>();
        adapter=new LoaiphongAdapter(LoaiPhong.this,R.layout.layout_listloaiphong,loaiphongArrayList);
        listLoaiphong.setAdapter(adapter);

//        Intent intent=getIntent();
//        tkdn=intent.getStringExtra("id_tkdn1");

        Hienthi(urlData);
    }
    private void Hienthi(String url){
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                loaiphongArrayList.clear();
                for (int i=0;i<response.length();i++){
                    try {
                        JSONObject object =  response.getJSONObject(i);
                        loaiphongArrayList.add(new LoaiphongGET(
                                object.getInt("id_loai"),
                                object.getString("tenloai"),
                                object.getString("dientich"),
                                object.getInt("soluong_khach"),
                                object.getString("doituong"),
                                object.getInt("gia_phong"),
                                object.getString("luu_y")
                        ));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                adapter.notifyDataSetChanged();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoaiPhong.this, "Lỗi tải nội dung", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toanha, menu);
        MenuItem timkiem=menu.findItem(R.id.timkiem);
        SearchView search= (SearchView) timkiem.getActionView();


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id){
            case R.id.add:
                Intent intent = new Intent(LoaiPhong.this, AddLoaiphong.class);
                startActivity(intent);
                break;
        }
        ;
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void DelLP(int idlp){
        RequestQueue requestQueue=Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlDel, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.trim().equals("success")){
                    Toast.makeText(LoaiPhong.this, "Xóa thành công", Toast.LENGTH_SHORT).show();
                    Hienthi(urlData);
                }else{
                    Toast.makeText(LoaiPhong.this, "Xóa không thành công", Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("id_loai", String.valueOf(idlp));
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
