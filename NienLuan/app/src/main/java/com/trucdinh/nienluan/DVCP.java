package com.trucdinh.nienluan;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DVCP extends AppCompatActivity {
    ListView listdvcp;
    ArrayList<dvcpGET> dvcpArrayList;
    dvcpAdapter adapter;
    String urlData="http://192.168.1.7/nienluan/listDVCP.php";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_dvcp);
        listdvcp =(ListView)findViewById(R.id.listdvcp);
        dvcpArrayList= new ArrayList<>();
        adapter=new dvcpAdapter(DVCP.this,R.layout.list_dichvucp,dvcpArrayList);
        listdvcp.setAdapter(adapter);
        Hienthi(urlData);
    }
    private void Hienthi(String url){
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                dvcpArrayList.clear();
                for (int i=0;i<response.length();i++){
                    try {
                        JSONObject object =  response.getJSONObject(i);
                        dvcpArrayList.add(new dvcpGET(
                                object.getInt("id_cp"),
                                object.getString("tendv"),
                                object.getString("donvi"),
                                object.getString("anh_daidien")

                        ));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                adapter.notifyDataSetChanged();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(DVCP.this, "Lỗi tải nội dung", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }
}
