package com.trucdinh.nienluan;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.trucdinh.nienluan.DaidienGET;
import com.trucdinh.nienluan.R;

import java.util.List;

public class DaidienAdapter extends BaseAdapter {
    private DaiDien context;
    private int layout;
    private List<DaidienGET> daidienList;
    public DaidienAdapter(DaiDien context,int layout, List<DaidienGET> daidienList){
        this.context=context;
        this.layout=layout;
        this.daidienList=daidienList;
    }



    @Override
    public int getCount() {
        return daidienList.size();
    }
    private class ViewHolder{
        TextView hoten, sdt, gioitinh;
        ImageView imEdit, imDel;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view ==null){
            holder = new ViewHolder();
            LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(layout,null);
            holder.hoten   =(TextView) view.findViewById(R.id.txthoten);
            holder.sdt =(TextView) view.findViewById(R.id.txtsdt);
            holder.gioitinh=(TextView) view.findViewById(R.id.txtgioitinh);

            holder.imDel   =(ImageView) view.findViewById(R.id.imDel);
            holder.imEdit  =(ImageView) view.findViewById(R.id.imEdit);
            view.setTag(holder);
        }else {
            holder=(ViewHolder) view.getTag();
        }
        DaidienGET daidien =daidienList.get(i);
        holder.hoten.setText(""+daidien.getHoten());
        holder.sdt.setText("SĐT:  " + daidien.getSdt());
        holder.gioitinh.setText(""+daidien.getGioitinh());

        //bat  su kiện xóa và sửa
        holder.imEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,UpdateDaidien.class);
                intent.putExtra("dataDaidien", daidien);
                context.startActivity(intent);
            }
        });
        holder.imDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Xacnhanxoa(daidien.getHoten(), daidien.getId_tk());
            }
        });
        return view;
    }
    private void Xacnhanxoa (String ten, int id){
        AlertDialog.Builder dialogXoa = new AlertDialog.Builder(context);
        dialogXoa.setMessage("Bạn có chắc muốn xóa " +ten+ "?");
        dialogXoa.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                context.DelDaidien(id);
            }
        });
        dialogXoa.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogXoa.show();
    }
}
