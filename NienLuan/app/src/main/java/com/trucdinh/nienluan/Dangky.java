package com.trucdinh.nienluan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class Dangky extends AppCompatActivity {
    EditText edsdtdk,edmkdk,edhotendk,edcmdk,edgtdk,ednsdk,eddcdk,edmknl;
    Button btnDangky;
    String urlDangky ="http://192.168.1.7/nienluan/Dangky.php";
    int phanquyen=1;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_dangky);
        edsdtdk = (EditText) findViewById(R.id.edsdtdk);
        edmkdk = (EditText) findViewById(R.id.edmkdk);
        edhotendk = (EditText) findViewById(R.id.edhotendk);
        edcmdk = (EditText) findViewById(R.id.edcmdk);
        edgtdk = (EditText) findViewById(R.id.edgtdk);
        ednsdk = (EditText) findViewById(R.id.ednsdk);
        eddcdk = (EditText) findViewById(R.id.eddcdk);
        btnDangky = (Button) findViewById(R.id.btnDangky);
        edmknl = (EditText) findViewById(R.id.edmknl);

        btnDangky.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dangky(urlDangky);
            }
        });
    }
    private void Dangky (String url){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.trim().equals("true")){
                    Toast.makeText(Dangky.this, "Thêm lỗi", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(Dangky.this, "Thêm thành công!", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Dangky.this,MainActivity.class));
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //bao loi cho nguoi lap trinh
                        Toast.makeText(Dangky.this, "Xảy ra lỗi lập trình", Toast.LENGTH_SHORT).show();
                        Log.d("AAA","Lỗi!\n"  + error.toString());
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("sdt",edsdtdk.getText().toString().trim());
                params.put("matkhau",edmkdk.getText().toString().trim());
                params.put("phan_quyen",String.valueOf(phanquyen));
                params.put("hoten",edhotendk.getText().toString().trim());
                params.put("cmnd",edcmdk.getText().toString().trim());
                params.put("gioitinh",edgtdk.getText().toString().trim());
                params.put("ngaysinh",ednsdk.getText().toString().trim());
                params.put("diachi",eddcdk.getText().toString().trim());
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

}
