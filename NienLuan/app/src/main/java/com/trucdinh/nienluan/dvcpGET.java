package com.trucdinh.nienluan;

import java.io.Serializable;

public class dvcpGET implements Serializable {
    public dvcpGET(int id_cp, String tendv, String donvi, String anh_daidien) {
        this.id_cp = id_cp;
        this.tendv = tendv;
        this.donvi = donvi;
        this.anh_daidien = anh_daidien;
    }

    public int getId_cp() {
        return id_cp;
    }

    public void setId_cp(int id_cp) {
        this.id_cp = id_cp;
    }

    public String getTendv() {
        return tendv;
    }

    public void setTendv(String tendv) {
        this.tendv = tendv;
    }

    public String getDonvi() {
        return donvi;
    }

    public void setDonvi(String donvi) {
        this.donvi = donvi;
    }

    public String getAnh_daidien() {
        return anh_daidien;
    }

    public void setAnh_daidien(String anh_daidien) {
        this.anh_daidien = anh_daidien;
    }

    int id_cp;
    String tendv;
    String donvi;
    String anh_daidien;
}
