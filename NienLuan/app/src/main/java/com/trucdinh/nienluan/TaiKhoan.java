package com.trucdinh.nienluan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaiKhoan extends AppCompatActivity {
    TextView ttsdt,ttcmnd,ttgt,ttngay,ttdc,xinchao;
    Button  EditTT;
    public static String  tkdn1;
    String urlData="http://192.168.1.7/nienluan/getTaikhoan.php";
    ListView listTaikhoan;
    ArrayList<DaidienGET> daidienArrayList;
    TaikhoanAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_taikhoan);
        Intent intent=getIntent();
        tkdn1=intent.getStringExtra("id_tkdn1");
        //Toast.makeText(TaiKhoan.this, ""+tkdn1, Toast.LENGTH_SHORT).show();

        ttsdt=(TextView) findViewById(R.id.ttsdt);
        ttcmnd=(TextView) findViewById(R.id.ttcmnd);
        ttgt=(TextView) findViewById(R.id.ttgt);
        ttngay=(TextView) findViewById(R.id.ttngay);
        ttdc=(TextView) findViewById(R.id.ttdc);
        xinchao=(TextView) findViewById(R.id.xinchao);
        EditTT=(Button) findViewById(R.id.EditTT);
        listTaikhoan =(ListView)findViewById(R.id.listtaikhoan);
        daidienArrayList= new ArrayList<>();
        adapter=new TaikhoanAdapter(TaiKhoan.this,R.layout.layout_listtaikhoan,daidienArrayList);
        listTaikhoan.setAdapter(adapter);
        Hienthi(urlData);

    }
    private void Hienthi (String url) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Request.Method.GET,url,null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        daidienArrayList.clear();
                        //Toast.makeText(DaiDien.this, response.toString(), Toast.LENGTH_SHORT).show();
                        for(int i=0;i< response.length();i++){

                            try {
                                JSONObject object =  response.getJSONObject(i);
                                daidienArrayList.add(new DaidienGET(
                                        object.getInt("id_tk"),
                                        object.getString("sdt"),
                                        object.getString("matkhau"),
                                        object.getInt("phan_quyen"),
                                        object.getString("hoten"),
                                        object.getString("cmnd"),
                                        object.getString("gioitinh"),
                                        object.getString("ngaysinh"),
                                        object.getString("diachi")
                                ));
//
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TaiKhoan.this, "Lỗi", Toast.LENGTH_SHORT).show();

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params=new HashMap<>();
                params.put("id_tk", String.valueOf(tkdn1));
                return params;
            }
        };
        requestQueue.add(jsonArrayRequest);
    }
}
