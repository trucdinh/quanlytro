package com.trucdinh.nienluan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class UpdatePhong extends AppCompatActivity {
    EditText udtenphong,udluuyPhong;
    Button btnEditphong,huyphong;
    Spinner udspinLoaiphong;
    CheckBox udcbtinhtrangco,udcbtinhtrangkhong;
    int idkt=1, idlp=9, tinhtrang,id;
    String urlUpdate="http://192.168.1.7/nienluan/updatePhong.php";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_updatephong);
        Intent intent=getIntent();
        PhongGET phongGET=(PhongGET) intent.getSerializableExtra("dataPhong");
        udspinLoaiphong=(Spinner)findViewById(R.id.udspinLoaiphong);
        udtenphong=(EditText)findViewById(R.id.udtenphong);
        udluuyPhong=(EditText)findViewById(R.id.udluuyPhong);
        btnEditphong=(Button) findViewById(R.id.btnEditphong);
        huyphong=(Button)findViewById(R.id.huyphong);
        udcbtinhtrangco=(CheckBox) findViewById(R.id.udcbtinhtrangco);
        udcbtinhtrangkhong=(CheckBox) findViewById(R.id.udcbtinhtrangkhong);
        id=phongGET.getId_phong();

        udtenphong.setText(phongGET.getTenphong());
        udluuyPhong.setText(phongGET.getLuu_y());

        String arr[] = {
                "Có gác",
                "Không gác",
                "Mini house"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item,
                arr);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        udspinLoaiphong.setAdapter(adapter);
        udspinLoaiphong.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //selection.setText(arr[2]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        udcbtinhtrangco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((CheckBox) v).isChecked();
                // Check which checkbox was clicked
                if (checked){
                    tinhtrang=1;
                }
                else{
                    tinhtrang=0;
                }
            }
        });
        udcbtinhtrangkhong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((CheckBox) v).isChecked();
                // Check which checkbox was clicked
                if (checked){
                    tinhtrang=0;
                }
                else{
                    tinhtrang=1;
                }
            }
        });
        btnEditphong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CapnhatPhong(urlUpdate);
            }
        });
        huyphong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UpdatePhong.this,Phong.class));
            }
        });
    }
    private void CapnhatPhong (String  url){
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.trim().equals("success")){
                    Toast.makeText(UpdatePhong.this, "Lỗi cập nhật", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(UpdatePhong.this, "Cập nhật thành công", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(UpdatePhong.this,Phong.class));

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params=new HashMap<>();
                params.put("id_phong", String.valueOf(id));
                params.put("id_kt",String.valueOf(idkt));
                params.put("id_loai", String.valueOf(idlp));
                params.put("tenphong",udtenphong.getText().toString().trim());

                params.put("tinhtrang",String.valueOf(tinhtrang));
                //params.put("hinhanh",udluuy.getText().toString().trim());
                params.put("luu_y",udluuyPhong.getText().toString().trim());
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
