package com.trucdinh.nienluan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class UpdateLoaiphong extends AppCompatActivity {
    EditText udtenloai,uddientich,udsoluongkhach,uddoituong,udluuy,udgia;
    Button  btnCapnhatLoai,btnHuy;
    int id, quangcao,soluongkhach,gia;
    String urlUpdate="http://192.168.1.7/nienluan/updateLoaiphong.php";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_updateloaiphong);
        Intent intent=getIntent();
        LoaiphongGET loaiphongGET=(LoaiphongGET) intent.getSerializableExtra("dataLoaiphong");
        udtenloai=(EditText) findViewById(R.id.udtenloai);
        uddientich=(EditText) findViewById(R.id.uddientich);
        udsoluongkhach=(EditText) findViewById(R.id.udsoluongkhach);
        uddoituong=(EditText) findViewById(R.id.uddoituong);
        udluuy=(EditText) findViewById(R.id.udluuy);
        udgia=(EditText)findViewById(R.id.udgia);
        btnCapnhatLoai=(Button) findViewById(R.id.btnCapnhatLoai);
        btnHuy=(Button) findViewById(R.id.btnHuy);
        id=loaiphongGET.getId_loai();

        udtenloai.setText(loaiphongGET.getTenloai());
        uddientich.setText(loaiphongGET.getDientich());
        soluongkhach=loaiphongGET.getSoluong_khach();
        udsoluongkhach.setText(String.valueOf(soluongkhach));
        uddoituong.setText(loaiphongGET.getDoituong());
        gia=loaiphongGET.getGia_phong();
        udgia.setText(String.valueOf(gia));
        udluuy.setText(loaiphongGET.getLuu_y());


        btnCapnhatLoai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(udtenloai.getText().toString().isEmpty()){
                    Toast.makeText(UpdateLoaiphong.this, "Bạn chưa nhập tên loại phòng!", Toast.LENGTH_SHORT).show();
                    udtenloai.requestFocus();
                }else if(uddientich.getText().toString().isEmpty()){
                    Toast.makeText(UpdateLoaiphong.this, "Bạn chưa nhập diện tích", Toast.LENGTH_SHORT).show();
                    uddientich.requestFocus();
                }else if(udsoluongkhach.getText().toString().isEmpty()){
                    Toast.makeText(UpdateLoaiphong.this, "Bạn chưa nhập số lượng khách", Toast.LENGTH_SHORT).show();
                    udsoluongkhach.requestFocus();
                }else if(uddoituong.getText().toString().isEmpty()){
                    Toast.makeText(UpdateLoaiphong.this, "Bạn chưa nhập đối tượng thuê phòng", Toast.LENGTH_SHORT).show();
                    uddoituong.requestFocus();
                }else {
                    CapnhatLoaiphong(urlUpdate);
                }
            }

        });
        btnHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UpdateLoaiphong.this,LoaiPhong.class));
            }
        });

    }
    private void CapnhatLoaiphong (String  url){
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.trim().equals("success")){
                    Toast.makeText(UpdateLoaiphong.this, "Cập nhật thành công", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(UpdateLoaiphong.this,LoaiPhong.class));

                }else{
                    Toast.makeText(UpdateLoaiphong.this, "Lỗi cập nhật", Toast.LENGTH_SHORT).show();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params=new HashMap<>();
                params.put("id_loai", String.valueOf(id));
                params.put("tenloai",udtenloai.getText().toString().trim());
                params.put("dientich", uddientich.getText().toString().trim());
                params.put("soluong_khach",udsoluongkhach.getText().toString().trim());

                params.put("doituong",uddoituong.getText().toString().trim());
                params.put("gia_phong",udgia.getText().toString().trim());
                params.put("luu_y",udluuy.getText().toString().trim());

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}

