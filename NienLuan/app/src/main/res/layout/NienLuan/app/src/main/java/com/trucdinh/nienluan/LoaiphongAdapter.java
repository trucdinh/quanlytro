package com.trucdinh.nienluan;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

public class LoaiphongAdapter extends BaseAdapter {
    public LoaiphongAdapter(Context context, int layout, List<LoaiphongGET> loaiphongList) {
        this.context = context;
        this.layout = layout;
        this.loaiphongList = loaiphongList;
    }

    private Context context;
    private int layout;
    private List<LoaiphongGET> loaiphongList;
    @Override
    public int getCount() {
        return loaiphongList.size();
    }
    private class ViewHolder{
        TextView tenloai, soluong_khach, dientich, doituong;
        ImageView imEdit, imDel;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LoaiphongAdapter.ViewHolder holder;
        if(view ==null){
            holder = new ViewHolder();
            LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(layout,null);
            holder.tenloai   =(TextView) view.findViewById(R.id.txttenloaiphong);
            holder.soluong_khach =(TextView) view.findViewById(R.id.txtsoluongkhach);
            holder.dientich   =(TextView) view.findViewById(R.id.txtdientich);
            holder.doituong =(TextView) view.findViewById(R.id.txtdoituong);
            //  holder.sophongtro =(TextView) view.findViewById(R.id.sophongtro);

            holder.imDel   =(ImageView) view.findViewById(R.id.imDelkt);
            holder.imEdit  =(ImageView) view.findViewById(R.id.imEditkt);
            view.setTag(holder);
        }else {
            holder=(LoaiphongAdapter.ViewHolder) view.getTag();
        }
        LoaiphongGET loaiphongGET =loaiphongList.get(i);
        holder.tenloai.setText(""+loaiphongGET.getTenloai());
        holder.soluong_khach.setText("SL tối đa: "+ loaiphongGET.getSoluong_khach());
        holder.dientich.setText("Diện tích: "+loaiphongGET.getDientich());
        holder.doituong.setText("Đối tương: "+ loaiphongGET.getDoituong());

        return view;
    }
}
