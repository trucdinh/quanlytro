package com.trucdinh.nienluan;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

public class TrangChu extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar_tc;
    ImageButton btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn10, btn11, btn12;
public static String tkdn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_trangchu);
        toolbar_tc = (Toolbar) findViewById(R.id.toolbar_tc);
        setSupportActionBar(toolbar_tc);
        //loai bo tieu đe co  san
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //toolbar_tc.inflateMenu(R.menu.menu_tc);
        btn1 = (ImageButton) findViewById(R.id.imbtn1);
        btn2 = (ImageButton) findViewById(R.id.imbtn2);
        btn3 = (ImageButton) findViewById(R.id.imbtn3);
        btn4 = (ImageButton) findViewById(R.id.imbtn4);
        btn5 = (ImageButton) findViewById(R.id.imbtn5);
        btn6 = (ImageButton) findViewById(R.id.imbtn6);
        btn7 = (ImageButton) findViewById(R.id.imbtn7);
        btn8 = (ImageButton) findViewById(R.id.imbtn8);
        btn9 = (ImageButton) findViewById(R.id.imbtn9);
        btn10 = (ImageButton) findViewById(R.id.imbtn10);
        btn11 = (ImageButton) findViewById(R.id.imbtn11);
        btn12 = (ImageButton) findViewById(R.id.imbtn12);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn7.setOnClickListener(this);

        //nhận id
        Intent intent=getIntent();
        tkdn=intent.getStringExtra("id_tkdn");
        Toast.makeText(TrangChu.this, tkdn, Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imbtn1:
                Intent intent1 = new Intent(TrangChu.this, ToaNha.class);
                intent1.putExtra("id_tkdn1",tkdn);
                startActivity(intent1);

                break;
            case R.id.imbtn2:
                Intent intent2 = new Intent(TrangChu.this, Phong.class);
                intent2.putExtra("id_tkdn1",tkdn);
                startActivity(intent2);
                break;
            case R.id.imbtn3:
                Intent intent3 = new Intent(TrangChu.this, LoaiPhong.class);
                startActivity(intent3);
                break;
            case R.id.imbtn7:
                Intent intent4 = new Intent(TrangChu.this, DaiDien.class);
                startActivity(intent4);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tc, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
//thao tác với các item trong menu
        switch (id) {
            case R.id.xem:
                Toast.makeText(TrangChu.this, "Bạn vừa ấn vào" + item.getTitle(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.dangxuat:
                Toast.makeText(TrangChu.this, "Bạn vừa ấn vào" + item.getTitle(), Toast.LENGTH_SHORT).show();
                break;
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
