package com.trucdinh.nienluan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class UpdateKhutro extends AppCompatActivity {
    EditText udtenkhutro,uddiachitro,udsophong,udmota,udchukynoptien;
    CheckBox udcheckquangcao;
    Button  btnCapnhat,btnHuy;
    int id, quangcao;
    String urlInsert="http://192.168.1.5/nienluan/updateKhutro.php";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_updatekhutro);
        Intent intent=getIntent();
        KhutroGET khutroGET=(KhutroGET) intent.getSerializableExtra("dataKhutro");
        udcheckquangcao=(CheckBox)findViewById(R.id.udcheckquangcao);
        udchukynoptien=(EditText) findViewById(R.id.udchukynoptien);
        uddiachitro=(EditText) findViewById(R.id.uddiachitro);
        udtenkhutro=(EditText) findViewById(R.id.udtenkhutro);
        udsophong=(EditText) findViewById(R.id.udsophong);
        udmota=(EditText) findViewById(R.id.udmota);
        btnCapnhat=(Button) findViewById(R.id.btnCapnhat);
        btnHuy=(Button) findViewById(R.id.btnHuy);
        id=khutroGET.getId_kt();
        //udcheckquangcao.setText(khutroGET.getQuangcao());
        udchukynoptien.setText(khutroGET.getChuky_noptien());
        uddiachitro.setText(khutroGET.getDiachi());
        udtenkhutro.setText(khutroGET.getTen_kt());
       // udsophong.setText(khutroGET.getSophong());
        udmota.setText(khutroGET.getMota());
        udcheckquangcao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((CheckBox) v).isChecked();
                // Check which checkbox was clicked
                if (checked){
                    quangcao=1;
                }
                else{
                    quangcao=0;
                }
            }
        });
        btnCapnhat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(udchukynoptien.getText().toString().isEmpty()){
                    Toast.makeText(UpdateKhutro.this, "Bạn chưa nhập chu kỳ nộp tiền", Toast.LENGTH_SHORT).show();
                    udchukynoptien.requestFocus();
                }else if(uddiachitro.getText().toString().isEmpty()){
                    Toast.makeText(UpdateKhutro.this, "Bạn chưa nhập Địa chỉ", Toast.LENGTH_SHORT).show();
                    uddiachitro.requestFocus();
                }else if(udtenkhutro.getText().toString().isEmpty()){
                    Toast.makeText(UpdateKhutro.this, "Bạn chưa nhập Tên khu trọ", Toast.LENGTH_SHORT).show();
                    udtenkhutro.requestFocus();
                }else if(udsophong.getText().toString().isEmpty()){
                    Toast.makeText(UpdateKhutro.this, "Bạn chưa nhập Số phòng", Toast.LENGTH_SHORT).show();
                    udsophong.requestFocus();
                }else if(udmota.getText().toString().isEmpty()){
                    Toast.makeText(UpdateKhutro.this, "Bạn chưa nhập Mô tả", Toast.LENGTH_SHORT).show();
                    udmota.requestFocus();
                }else {
                    CapnhatKhutro(urlInsert);
                }
            }

        });
        btnHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UpdateKhutro.this,ToaNha.class));
            }
        });

    }
    private void CapnhatKhutro (String  url){
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.trim().equals("success")){
                    Toast.makeText(UpdateKhutro.this, "Lỗi cập nhật", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(UpdateKhutro.this, "Cập nhật thành công", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(UpdateKhutro.this,ToaNha.class));

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params=new HashMap<>();
                params.put("id_kt", String.valueOf(id));
                params.put("ten_kt",udtenkhutro.getText().toString().trim());
                params.put("diachi", uddiachitro.getText().toString().trim());
                params.put("sophong",udsophong.getText().toString().trim());
                params.put("quangcao",String.valueOf(quangcao));
                params.put("mota",udmota.getText().toString().trim());
                params.put("chuky_noptien",udchukynoptien.getText().toString().trim());

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
    }

