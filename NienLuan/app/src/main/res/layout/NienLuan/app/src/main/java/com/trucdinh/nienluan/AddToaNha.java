package com.trucdinh.nienluan;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.trucdinh.nienluan.Model.NguoiDung;

import java.util.HashMap;
import java.util.Map;

public class AddToaNha extends AppCompatActivity {
  EditText edtenkhutro, eddiachitro, edsophong, edmota, edchukynoptien;
  CheckBox checkquangcao;
  Button btnThem, btnHuy;
  private  NguoiDung nguoiDung;
    public static String tkdn;
  String urlInsert="http://192.168.1.5/nienluan/insertKhutro.php";
  int quangcao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_addtoanha);

        edchukynoptien=(EditText) findViewById(R.id.edchukynoptien);
        eddiachitro=(EditText) findViewById(R.id.eddiachitro);
        edtenkhutro=(EditText) findViewById(R.id.edtenkhutro);
        edsophong=(EditText) findViewById(R.id.edsophong);
        edmota=(EditText) findViewById(R.id.edmota);
        checkquangcao=(CheckBox) findViewById(R.id.checkquangcao);
        btnHuy=(Button) findViewById(R.id.btnHuy);
        btnThem=(Button) findViewById(R.id.btnThem);

        //nhan id
        Intent intent=getIntent();
        tkdn=intent.getStringExtra("id_tkdn2");
        //Toast.makeText(AddToaNha.this, tkdn, Toast.LENGTH_SHORT).show();
btnHuy.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent1=new Intent(AddToaNha.this,ToaNha.class);
        startActivity(intent1);
    }
});
        btnThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edchukynoptien.getText().toString().isEmpty()){
                    Toast.makeText(AddToaNha.this, "Bạn chưa nhập Chy kỳ nộp tiền!", Toast.LENGTH_SHORT).show();
                    edchukynoptien.requestFocus();
                }else if(eddiachitro.getText().toString().isEmpty()){
                    Toast.makeText(AddToaNha.this, "Bạn chưa nhập Địa chỉ trọ", Toast.LENGTH_SHORT).show();
                    eddiachitro.requestFocus();
                }else if(edtenkhutro.getText().toString().isEmpty()){
                    Toast.makeText(AddToaNha.this, "Bạn chưa nhập Tên khu trọ", Toast.LENGTH_SHORT).show();
                    edtenkhutro.requestFocus();
                }else if(edsophong.getText().toString().isEmpty()){
                    Toast.makeText(AddToaNha.this, "Bạn chưa nhập Số phòng", Toast.LENGTH_SHORT).show();
                    edsophong.requestFocus();
                }else {
                    ThemKhutro(urlInsert);
                }
            }
        });
        checkquangcao.setOnClickListener(new View.OnClickListener() {
             @Override
                 public void onClick(View v) {
                 boolean checked = ((CheckBox) v).isChecked();
                 // Check which checkbox was clicked
                 if (checked){
                     quangcao=1;
                 }
                 else{
                    quangcao=0;
                 }
            }
        });



    }
    private void ThemKhutro (String url){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.trim().equals("success")){
                    Toast.makeText(AddToaNha.this, "Thêm thành công!", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(AddToaNha.this,ToaNha.class));

                }else{
                    Toast.makeText(AddToaNha.this, "Thêm lỗi", Toast.LENGTH_SHORT).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //bao loi cho nguoi lap trinh
                        Toast.makeText(AddToaNha.this, "Xảy ra lỗi lập trình", Toast.LENGTH_SHORT).show();
                        Log.d("AAA","Lỗi!\n"  + error.toString());
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("ten_kt",edtenkhutro.getText().toString().trim());
                params.put("diachi",eddiachitro.getText().toString().trim());
                params.put("sophong",edsophong.getText().toString().trim());
                params.put("quangcao",String.valueOf(quangcao));
                params.put("mota",edmota.getText().toString().trim());
                params.put("chuky_noptien",edchukynoptien.getText().toString().trim());
                params.put("id_tk",String.valueOf(tkdn));

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }


    }


