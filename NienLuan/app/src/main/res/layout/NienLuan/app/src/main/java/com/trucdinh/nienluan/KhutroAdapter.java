package com.trucdinh.nienluan;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class KhutroAdapter extends BaseAdapter {
    private ToaNha context;
    private int layout;
    private List<KhutroGET> khutroList;

    public KhutroAdapter(ToaNha context, int layout, List<KhutroGET> khutroList) {
        this.context = context;
        this.layout = layout;
        this.khutroList = khutroList;
    }

    @Override
    public int getCount() {
        return khutroList.size();
    }
    private class ViewHolder{
        TextView tenkhutro, diachi;
        ImageView imEdit,imDel;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        KhutroAdapter.ViewHolder holder;
        if(view ==null){
            holder = new ViewHolder();
            LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(layout,null);
            holder.tenkhutro   =(TextView) view.findViewById(R.id.tenkhutro);
           holder.diachi =(TextView) view.findViewById(R.id.diachitro);
          //  holder.sophongtro =(TextView) view.findViewById(R.id.sophongtro);

            holder.imDel   =(ImageView) view.findViewById(R.id.imDelkt);
            holder.imEdit  =(ImageView) view.findViewById(R.id.imEditkt);
            view.setTag(holder);
        }else {
            holder=(KhutroAdapter.ViewHolder) view.getTag();
        }
        KhutroGET khutroGET =khutroList.get(i);
        holder.tenkhutro.setText(""+khutroGET.getTen_kt());
        holder.diachi.setText(""+ khutroGET.getDiachi());
       // holder.sophongtro.setText("Số phòng:"+ khutroGET.getSophong());

        //bat  su kiện xóa và sửa
        holder.imEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,UpdateKhutro.class);
                intent.putExtra("dataKhutro", khutroGET);
                context.startActivity(intent);
            }
        });
        holder.imDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Xacnhanxoa(khutroGET.getTen_kt(), khutroGET.getId_kt());
            }
        });
        return view;
    }
    private void Xacnhanxoa (String ten, int idkt){
        AlertDialog.Builder dialogXoa = new AlertDialog.Builder(context);
        dialogXoa.setMessage("Bạn có chắc muốn xóa " +ten+ "?");
        dialogXoa.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                context.DelKhutro(idkt);
            }
        });
        dialogXoa.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogXoa.show();
    }
}
