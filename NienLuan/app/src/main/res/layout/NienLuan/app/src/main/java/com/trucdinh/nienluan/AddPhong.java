package com.trucdinh.nienluan;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

public class AddPhong extends AppCompatActivity {
    Toolbar toolbar_addphong;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_addphong);
        toolbar_addphong=(Toolbar) findViewById(R.id.toolbar_addphong);
        setSupportActionBar(toolbar_addphong);
        //loai bo tieu đe co  san
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
}
