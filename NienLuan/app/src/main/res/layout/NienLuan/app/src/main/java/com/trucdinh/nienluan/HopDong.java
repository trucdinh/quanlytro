package com.trucdinh.nienluan;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class HopDong extends AppCompatActivity {
    Toolbar toolbar_hopdong;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_hopdong);
        toolbar_hopdong=(Toolbar) findViewById(R.id.toolbar_hopdong);
        setSupportActionBar(toolbar_hopdong);
        //loai bo tieu đe co  san
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toanha, menu);
        MenuItem timkiem=menu.findItem(R.id.timkiem);
        SearchView search= (SearchView) timkiem.getActionView();


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id){
            case R.id.add:
                Intent intent = new Intent(HopDong.this, AddHopdong.class);
                startActivity(intent);
                break;
        }
        ;
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
