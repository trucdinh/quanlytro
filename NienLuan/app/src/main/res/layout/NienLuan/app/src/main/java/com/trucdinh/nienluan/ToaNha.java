package com.trucdinh.nienluan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ToaNha extends AppCompatActivity {
    Toolbar toolbar_toanha;
    ListView listKhutro;
    ArrayList<KhutroGET> khutroArrayList;
    KhutroAdapter adapter;
    String urlData="http://192.168.1.5/nienluan/listKhutro.php";
    String urlDel="http://192.168.1.5/nienluan/DelKhutro.php";
    public static String tkdn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layouot_toanha);
        toolbar_toanha=(Toolbar) findViewById(R.id.toolbar_toanha);
        setSupportActionBar(toolbar_toanha);
        //loai bo tieu đe co  san
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        listKhutro =(ListView)findViewById(R.id.listKhutro);
        khutroArrayList= new ArrayList<>();
        adapter=new KhutroAdapter(ToaNha.this,R.layout.layout_listtoanha,khutroArrayList);
        listKhutro.setAdapter(adapter);

        //nhanid
        Intent intent=getIntent();
        tkdn=intent.getStringExtra("id_tkdn1");
       // Toast.makeText(ToaNha.this, tkdn, Toast.LENGTH_SHORT).show();
        Hienthi(urlData);
    }
    private void Hienthi(String url){
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                khutroArrayList.clear();
                for (int i=0;i<response.length();i++){
                    try {
                        JSONObject object =  response.getJSONObject(i);
                        khutroArrayList.add(new KhutroGET(
                                object.getInt("id_kt"),
                                object.getString("ten_kt"),
                                object.getString("diachi"),
                                object.getInt("sophong"),
                                object.getInt("quangcao"),
                                object.getString("mota"),
                                object.getString("chuky_noptien")
                                ));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                adapter.notifyDataSetChanged();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ToaNha.this, "Lỗi tải nội dung", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toanha, menu);
        MenuItem timkiem=menu.findItem(R.id.timkiem);
        SearchView search= (SearchView) timkiem.getActionView();


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id){
            case R.id.add:
            Intent intent = new Intent(ToaNha.this, AddToaNha.class);
                intent.putExtra("id_tkdn2",tkdn);
            startActivity(intent);
                //Toast.makeText(ToaNha.this, tkdn, Toast.LENGTH_SHORT).show();
            break;
        }
       ;
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void DelKhutro(int idkt){
        RequestQueue requestQueue=Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlDel, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.trim().equals("success")){
                    Toast.makeText(ToaNha.this, "Xóa thành công", Toast.LENGTH_SHORT).show();
                    Hienthi(urlData);
                }else{
                    Toast.makeText(ToaNha.this, "Xóa không thành công", Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("id_kt", String.valueOf(idkt));
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

}
