package com.trucdinh.nienluan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;


public class AddDaiDien extends AppCompatActivity implements View.OnClickListener {
    Button btnThem, btnhuy;
    EditText edhoten, edcmnd, edngaysinh, edsdt, edmatkhau, edgioitinh, eddiachi;
    String a = "2";
String urlInsert ="http://192.168.1.5/nienluan/insertDaidien.php";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_adddaidien);
        btnhuy=(Button) findViewById(R.id.btnHuy) ;
        btnThem = (Button) findViewById(R.id.btnThem);
        edhoten = (EditText) findViewById(R.id.edtenkhutro);
        edcmnd = (EditText) findViewById(R.id.ednoidungtro);
        edngaysinh = (EditText) findViewById(R.id.edgiomocua);
        edsdt = (EditText) findViewById(R.id.eddiachitro);
        edmatkhau = (EditText) findViewById(R.id.edsophong);
        edgioitinh = (EditText) findViewById(R.id.edmota);
        eddiachi = (EditText) findViewById(R.id.edchukynoptien);
        btnThem.setOnClickListener(this);
        btnhuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AddDaiDien.this,DaiDien.class));
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(edsdt.getText().toString().isEmpty()){
            Toast.makeText(this, "Bạn chưa nhập Số điện thoại!", Toast.LENGTH_SHORT).show();
            edsdt.requestFocus();
        }else if(edhoten.getText().toString().isEmpty()){
            Toast.makeText(this, "Bạn chưa nhập Họ tên", Toast.LENGTH_SHORT).show();
            edhoten.requestFocus();
        }else if(edcmnd.getText().toString().isEmpty()){
            Toast.makeText(this, "Bạn chưa nhập Chứng minh", Toast.LENGTH_SHORT).show();
            edcmnd.requestFocus();
        }else if(edngaysinh.getText().toString().isEmpty()){
            Toast.makeText(this, "Bạn chưa nhập Ngày sinh", Toast.LENGTH_SHORT).show();
            edngaysinh.requestFocus();
        }else if(eddiachi.getText().toString().isEmpty()){
            Toast.makeText(this, "Bạn chưa nhập Địa chỉ", Toast.LENGTH_SHORT).show();
            eddiachi.requestFocus();
        }else if(edgioitinh.getText().toString().isEmpty()){
            Toast.makeText(this, "Bạn chưa nhập Giới tính", Toast.LENGTH_SHORT).show();
            edgioitinh.requestFocus();
        }else {
            ThemDaidien(urlInsert);
        }

        }
        private void ThemDaidien (String url){
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if(response.trim().equals("true")){
                        Toast.makeText(AddDaiDien.this, "Thêm lỗi", Toast.LENGTH_SHORT).show();

                    }else{
                        Toast.makeText(AddDaiDien.this, "Thêm thành công!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(AddDaiDien.this,DaiDien.class));
                    }

                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        //bao loi cho nguoi lap trinh
                            Toast.makeText(AddDaiDien.this, "Xảy ra lỗi lập trình", Toast.LENGTH_SHORT).show();
                            Log.d("AAA","Lỗi!\n"  + error.toString());
                        }
                    }
                    ){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> params = new HashMap<>();
                    params.put("sdt",edsdt.getText().toString().trim());
                    //params.put("matkhau",edmatkhau.getText().toString().trim());
                    params.put("phan_quyen",a);
                    params.put("hoten",edhoten.getText().toString().trim());
                    params.put("cmnd",edcmnd.getText().toString().trim());
                    params.put("gioitinh",edgioitinh.getText().toString().trim());
                    params.put("ngaysinh",edngaysinh.getText().toString().trim());
                    params.put("diachi",eddiachi.getText().toString().trim());

                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }

}
